#!/bin/bash

# Get the parameters from environment variables or arguments
name="$1"
org="$2"
env="$3"
version="$4"
type="$5"

# Print out the input parameters
echo "deploying proxy $name to $org, $env . Version is $version "
curl -u smitar@sidgs.com:Sidgs@123! -OJL https://sidgs.jfrog.io/artifactory/bdo-snapshots/com/sidgs/apigeex/$name-$version.zip
unzip $name-$version.zip
cd ..
cp $org.json $name/edge
cd $name/edge
if [ "$type" = "proxy" ]; then
    mvn clean package apigee-enterprise:deploy -Phybrid-apiproxy -Dorg=$org -Denv=$env -Dfile=sidgs-hybrid.json                    
elif [ "$type" = "sf" ]; then
    mvn clean package apigee-enterprise:deploy -Phybrid-sharedflow -Dorg=$org -Denv=$env -Dfile=sidgs-hybrid.json
elif [ "$type" = "targetserver" ]; then   
    mvn clean package apigee-config:targetservers -Phybrid-apiproxy -Dorg=$org -Denv=$env -Dfile=sidgs-hybrid.json -Dapigee.config.options=create
else                     
echo "give correct entity type and deployment type"                 
fi