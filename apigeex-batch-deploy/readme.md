Prerequisites:

Helm, K8 cluster

Steps to be followed

1) clone the repo - https://bitbucket.org/sidgs/apigee-cd/src/develop/

2) go to chart directory  - cd apigee-cd/apigeex-batch-deploy run command - helm install apigeex_batch_cd_chart ./apigeex_batch_cd_chart

3) to check the helm  installation - helm list

4) to check the job - kubectl get pods 

4) to check job logs kubectl logs -f pod_name